import { Injectable } from '@angular/core';

import { Address } from './address';

import { Headers, Http } from '@angular/http';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class AddressService {
  constructor(private http: Http) { }

  private addressesURL = 'http://localhost:8080/addresses';

  getAddresses(): Observable<Address[]> {
    return this.http.get(this.addressesURL).map(data => data.json());
  }
}