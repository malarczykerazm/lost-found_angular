export class Address {
    constructor(
      public id: number,
      public version: number,
      public creationTime: Date,
      public modificationTime: Date,
      public addressNumber: string,
      public street: string,
      public postcode: string,
      public city: string,
      public country: string,
    ) {  }
  }
