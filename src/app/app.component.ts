import { Component } from '@angular/core';

@Component({
  selector: 'lost-and-found-office',
  template: `
  <router-outlet></router-outlet>
  `
})
export class AppComponent {

}
