import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app.routing.module';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ClientComponent } from './client.component';
import { FoundItemComponent } from './found-item.component';
import { LostItemComponent } from './lost-item.component';
import { ClientItemWishComponent } from './client-item-wish.component';
import { DashboardComponent } from './dashboard.component';

import { ClientModalComponent } from './client.modal.component';
import { FoundItemModalComponent } from './found-item.modal.component';
import { LostItemModalComponent } from './lost-item.modal.component';
import { ClientItemWishModalComponent } from './client-item-wish.modal.component';

import { ClientService } from './client.service';
import { EnumService } from './enum.service';
import { FoundItemService } from './found-item.service';
import { LostItemService } from './lost-item.service';
import { AddressService } from './address.service';
import { ContactDataService } from './contact-data.service';
import { ClientItemWishService } from './client-item-wish.service';
import { ValidationProcessingService } from './app.validation-processing.service';

import { RatingModule } from './../../node_modules/ngx-rating/index.js';

import { ClientPipe } from './client.pipe';
import { FoundItemPipe } from './found-item.pipe';
import { LostItemPipe } from './lost-item.pipe';
import { ClientItemWishPipe } from './client-item-wish.pipe';

@NgModule({
  imports: [BrowserModule, FormsModule, AppRoutingModule, HttpModule, RatingModule],
  declarations: [AppComponent, ClientComponent, FoundItemComponent, LostItemComponent, ClientItemWishComponent, DashboardComponent, FoundItemModalComponent, LostItemModalComponent, ClientModalComponent, ClientItemWishModalComponent, ClientPipe, FoundItemPipe, LostItemPipe, ClientItemWishPipe],
  providers: [ClientService, EnumService, FoundItemService, LostItemService, AddressService, ContactDataService, ClientItemWishService, ValidationProcessingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
