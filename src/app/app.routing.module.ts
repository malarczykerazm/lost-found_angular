import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
 
import { ClientComponent }      from './client.component';
import { FoundItemComponent }      from './found-item.component';
import { LostItemComponent }      from './lost-item.component';
import { ClientItemWishComponent }      from './client-item-wish.component';
import { DashboardComponent }      from './dashboard.component';
 
const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'clients', component: ClientComponent },
  { path: 'found_items', component: FoundItemComponent },
  { path: 'lost_items', component: LostItemComponent },
  { path: 'wishlist', component: ClientItemWishComponent },
  { path: 'dashboard', component: DashboardComponent }
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}