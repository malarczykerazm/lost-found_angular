import { FoundItem } from './found-item';
import { Client } from './client';

import { ValidationProcessingService } from './app.validation-processing.service';

let input: string;
let expectedOutput: string;
let valProcService = new ValidationProcessingService();
let testWeight: number;
let testClient: Client;
let testItem: FoundItem;
let dummyClient: Client = new Client(null, null, null, null, null, null, null, null, null);

beforeEach(() => {
    testClient = new Client(1, null, null, null, null, null, null, null, null);
    testItem = new FoundItem(1, 0, new Date(), new Date(), "TestItem", "TestCategory", new Date(), "TestPlace", "TestColor", testWeight, null, null, dummyClient, null, dummyClient);
});

describe('Should', () => {
    it('replace underscores with spaces in provided string input', () => {
        //given
        input = "CREDIT_OR_DEBIT_CARD";
        expectedOutput = "CREDIT OR DEBIT CARD";

        //when
        let output = valProcService.replaceUnderScores(input);

        //then
        expect(output).toEqual(expectedOutput);
    });

    it('validate provided found item with GIVEN_AWAY status', () => {
        //given
        testItem.status = "GIVEN_AWAY";
        testItem.givenToClient = testClient;
        testItem.outOfDepositSince = new Date();

        //when //then
        expect(valProcService.validateFoundItem(testItem)).toBe(true);
    });

    it('not validate provided found item with GIVEN_AWAY status due to lack of client', () => {
        //given
        testItem.status = "GIVEN_AWAY";
        testItem.outOfDepositSince = new Date();

        //when //then
        expect(valProcService.validateFoundItem(testItem)).toBe(false);
    });

    it('not validate provided found item with GIVEN_AWAY status due to lack of date', () => {
        //given
        testItem.status = "GIVEN_AWAY";
        testItem.reportedByClient = testClient;

        //when //then
        expect(valProcService.validateFoundItem(testItem)).toBe(false);
    });

    it('validate provided found item with IN_DEPOSIT status', () => {
        //given
        testItem.status = "IN_DEPOSIT";

        //when //then
        expect(valProcService.validateFoundItem(testItem)).toBe(true);
    });

    it('not validate provided found item with IN_DEPOSIT status due to a client', () => {
        //given
        testItem.status = "IN_DEPOSIT";
        testItem.givenToClient = testClient;

        //when //then
        expect(valProcService.validateFoundItem(testItem)).toBe(false);
    });

    it('not validate provided found item with GIVEN_AWAY status due to a date', () => {
        //given
        testItem.status = "IN_DEPOSIT";
        testItem.outOfDepositSince = new Date();

        //when //then
        expect(valProcService.validateFoundItem(testItem)).toBe(false);
    });

    it('not validate provided found item with RETURNED_TO_OWNER due to the same client', () => {
        //given
        testItem.status = "RETURNED_TO_OWNER";
        testItem.outOfDepositSince = new Date();
        testItem.reportedByClient = testClient;
        testItem.givenToClient = testClient;

        //when //then
        expect(valProcService.validateFoundItem(testItem)).toBe(false);
    });

    it('validate provided found item due to awaiting time', () => {
        //given
        testItem.creationTime = new Date(new Date().valueOf() - 60001);

        //when //then
        expect(valProcService.validateFoundItemAwaitingTime(testItem)).toBe(true);
    });
    
    it('not validate provided found item due to awaiting time', () => {
        //given
        testItem.creationTime = new Date();

        //when //then
        expect(valProcService.validateFoundItemAwaitingTime(testItem)).toBe(false);
    });

});