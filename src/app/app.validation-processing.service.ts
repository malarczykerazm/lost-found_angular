import { Injectable } from '@angular/core';

import { FoundItem } from './found-item'
import { ClientItemWish } from './client-item-wish'

@Injectable()
export class ValidationProcessingService {

    replaceUnderScores(input: string): string {
        return input.replace(/_/g, " ");
    }

    validateFoundItem(selectedFoundItem: FoundItem): Boolean {
        if ((selectedFoundItem.status !== 'IN_DEPOSIT') && ((!selectedFoundItem.givenToClient.id) || (!selectedFoundItem.outOfDepositSince))) {
            return false;
        }
        if ((selectedFoundItem.status === 'IN_DEPOSIT') && ((selectedFoundItem.givenToClient.id) || (selectedFoundItem.outOfDepositSince))) {
            return false;
        }
        if ((selectedFoundItem.status === 'RETURNED_TO_OWNER') && (selectedFoundItem.givenToClient.id.toString() === selectedFoundItem.reportedByClient.id.toString())) {
            return false;
        }
        return true;
    }

    validateFoundItemAwaitingTime(selectedFoundItem: FoundItem): Boolean {
        return new Date().getTime() - new Date(selectedFoundItem.creationTime).getTime() > 60000;
    }

}