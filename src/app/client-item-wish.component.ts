import { Component, OnInit } from '@angular/core';

import { Client } from './client';
import { ClientItemWish } from './client-item-wish';
import { FoundItem } from "./found-item";
import { Address } from "./address";

import { ClientItemWishService } from './client-item-wish.service';
import { ClientService } from './client.service';
import { FoundItemService } from './found-item.service';
import { ValidationProcessingService } from './app.validation-processing.service';

import { ClientItemWishPipe } from './client-item-wish.pipe';

@Component({
  moduleId: module.id,
  templateUrl: `./../client-item-wish.component.html`,
  styleUrls: [`./../client-item-wish.component.css`, `./../app.common.modal.component.css`],
})

export class ClientItemWishComponent implements OnInit {

  subname = 'Wishlist';
  detailName = 'Details of the chosen wish';
  selectedClientItemWish: ClientItemWish;
  clientItemWishes: ClientItemWish[];
  clients: Client[];
  foundItems: FoundItem[];
  numberOfClientItemWishes: number;
  currentClientWishlist: ClientItemWish[] = [];

  idSorting: Boolean = true;
  clientSorting: Boolean = true;
  itemSorting: Boolean = true;
  ratingSorting: Boolean = true;


  constructor(
    private clientService: ClientService,
    private foundItemService: FoundItemService,
    private clientItemWishService: ClientItemWishService,
    private valProcService: ValidationProcessingService) { }

  getClientItemWishes(): void {
    this.clientItemWishService.getClientItemWishes().subscribe(cIW => this.clientItemWishes = cIW);
  }

  getFoundItems(): void {
    this.foundItemService.getFoundItems().subscribe(fi => this.foundItems = fi.sort((fi1, fi2) => {
      return fi1.id < fi2.id ? -1 : 1;
    }));
  }

  getClients(): void {
    this.clientService.getClients().subscribe(c => this.clients = c);
  }

  private deleteClientItemWish(id: number): void {
    this.clientItemWishService.deleteClientItemWish(id).subscribe(cIW => {
      this.clientItemWishes = this.clientItemWishes.filter(cIW => cIW.id !== id);
      this.selectedClientItemWish = null;
    });
  }

  deleteClientItemWishIfUserIsSure(id: number): void {
    if (window.confirm("Are you sure you want to remove the wish of ID: " + id + "?")) {
      this.deleteClientItemWish(id);
    }
  }

  prepareEmtyClientItemWishToSave(): void {
    this.selectedClientItemWish = new ClientItemWish(null, null, null, null, null, null, null);
    this.setDummiesWhereNoClientOrNoFoundItem(this.selectedClientItemWish);
  }

  onClientItemWishSelect(clientItemWish: ClientItemWish): void {
    this.selectedClientItemWish = clientItemWish;
    this.setDummiesWhereNoClientOrNoFoundItem(this.selectedClientItemWish);
  }

  offClientItemWishSelect(): void {
    this.selectedClientItemWish = null;
    this.currentClientWishlist = [];
  }

  addAndRefreshClientItemWish(clientItemWish: ClientItemWish): void {
    this.clientService.getClientWishlist(clientItemWish.client.id).subscribe(cWL => {
      this.clientItemWishService.saveClientItemWish(clientItemWish).subscribe(cIW => {
        this.clientItemWishes.push(cIW);
        this.selectedClientItemWish = cIW;
        this.setDummiesWhereNoClientOrNoFoundItem(this.selectedClientItemWish);
      });
    });
  }

  updateAndRefreshClientItemWish(clientItemWish: ClientItemWish): void {
    this.clientItemWishService.updateClientItemWish(clientItemWish).subscribe(cIW => {
      var arrayIndex: number = this.clientItemWishes.indexOf(this.clientItemWishes.find(cIW => cIW.id === clientItemWish.id));
      cIW.version++;
      this.clientItemWishes[arrayIndex] = cIW;
      this.selectedClientItemWish = cIW;
      this.setDummiesWhereNoClientOrNoFoundItem(this.selectedClientItemWish);
    });
  }

  justRefreshClientItemWish(clientItemWish: ClientItemWish): void {
    if (clientItemWish.id !== null) {
      this.clientItemWishService.getClientItemWishByID(clientItemWish.id).subscribe(cIW => {
        var arrayIndex: number = this.clientItemWishes.indexOf(this.clientItemWishes.find(cIW => cIW.id === clientItemWish.id));
        this.clientItemWishes[arrayIndex] = cIW;
        this.selectedClientItemWish = cIW;
        this.setDummiesWhereNoClientOrNoFoundItem(this.selectedClientItemWish);
      });
    }
  }

  saveOrUpdate(clientItemWish: ClientItemWish): void {
    if (clientItemWish.id === null) {
      this.addAndRefreshClientItemWish(clientItemWish);
    } else {
      this.updateAndRefreshClientItemWish(clientItemWish);
    }
  }

  setDummiesWhereNoClientOrNoFoundItem(clientItemWish: ClientItemWish): void {
    if (clientItemWish.client === null) {
      clientItemWish.client = new Client(null, null, null, null, null, null, null, null, null);
    }
    if (clientItemWish.foundItem === null) {
      clientItemWish.foundItem = new FoundItem(null, null, null, null, null, null, null, null, null, null, null);
    }
  }

  getCurrentClientWishlist(clientID: number): void {
    this.clientService.getClientWishlist(clientID).subscribe(cWL => {
      this.currentClientWishlist = cWL;
    });
  }

  checkIfItemAlreadyOccursOnCurrentClientWishlist(itemID: number): Boolean {
    return this.currentClientWishlist.filter(cIW => cIW.foundItem.id.toString() === itemID.toString()).length !== 0;
  }

  checkIfCurrentItemIsInDeposit(itemID: number): Boolean{
    if(itemID === null) {
      return false;
    }
    return this.foundItems.find(fi => fi.id.toString() === itemID.toString()).status === 'IN_DEPOSIT';
  }
  
  ngOnInit(): void {
    this.getClientItemWishes();
    this.getFoundItems();
    this.getClients();
  }

  sortByID() {
    if(this.idSorting) {
      this.clientItemWishes.sort((cIW1, cIW2) => {
        return cIW1.id < cIW2.id ? -1 : 1;
      });
      this.idSorting = false;
    } else {
      this.clientItemWishes.sort((cIW1, cIW2) => {
        return cIW1.id > cIW2.id ? -1 : 1;
      });
      this.idSorting = true;
    }
  }
  
  sortByClient() {
    if(this.clientSorting) {
      this.clientItemWishes.sort((cIW1, cIW2) => {
        return cIW1.client.id < cIW2.client.id ? -1 : 1;
      });
      this.clientSorting = false;
    } else {
      this.clientItemWishes.sort((cIW1, cIW2) => {
        return cIW1.client.id > cIW2.client.id ? -1 : 1;
      });
      this.clientSorting = true;
    }
  }
    
  sortByItem() {
    if(this.itemSorting) {
      this.clientItemWishes.sort((cIW1, cIW2) => {
        return cIW1.foundItem.id < cIW2.foundItem.id ? -1 : 1;
      });
      this.itemSorting = false;
    } else {
      this.clientItemWishes.sort((cIW1, cIW2) => {
        return cIW1.foundItem.id > cIW2.foundItem.id ? -1 : 1;
      });
      this.itemSorting = true;
    }
  }
     
  sortByRating() {
    if(this.ratingSorting) {
      this.clientItemWishes.sort((cIW1, cIW2) => {
        return cIW1.clientRating < cIW2.clientRating ? -1 : 1;
      });
      this.ratingSorting = false;
    } else {
      this.clientItemWishes.sort((cIW1, cIW2) => {
        return cIW1.clientRating > cIW2.clientRating ? -1 : 1;
      });
      this.ratingSorting = true;
    }
  }

}
