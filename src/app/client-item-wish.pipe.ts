import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { ClientItemWish } from './client-item-wish';

import { ValidationProcessingService } from './app.validation-processing.service';

@Pipe({ name: 'clientItemWishFilter' })
@Injectable()
export class ClientItemWishPipe implements PipeTransform {

    constructor(private vps: ValidationProcessingService) { }

    transform(clientItemWishes: any[], searchText: any): any {
        if (searchText == null || searchText === undefined || searchText == []) {
            return clientItemWishes;
        } else {
            return clientItemWishes.filter(clientItemWishes =>
                (clientItemWishes.foundItem.id.toString().includes(searchText.toLowerCase())) ||
                (clientItemWishes.foundItem.itemName.toLowerCase().includes(searchText.toLowerCase())) ||
                (this.vps.replaceUnderScores(clientItemWishes.foundItem.category).toLowerCase().includes(searchText.toLowerCase())) ||
                (this.vps.replaceUnderScores(clientItemWishes.foundItem.colour).toLowerCase().includes(searchText.toLowerCase())) ||
                (this.vps.replaceUnderScores(clientItemWishes.foundItem.status).toLowerCase().includes(searchText.toLowerCase())) ||
                (clientItemWishes.client.id.toString().includes(searchText.toLowerCase())) ||
                (clientItemWishes.client.firstname.toLowerCase().includes(searchText.toLowerCase())) ||
                (clientItemWishes.client.surname.toLowerCase().includes(searchText.toLowerCase())) ||
                (clientItemWishes.client.address.city.toLowerCase().includes(searchText.toLowerCase())));
        }
    }
}