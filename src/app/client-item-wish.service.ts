import { Injectable } from '@angular/core';

import { ClientItemWish } from './client-item-wish';

import { Headers, Http } from '@angular/http';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class ClientItemWishService {
  constructor(private http: Http) { }

  private headers = new Headers({ 'Content-Type': 'application/json' });

  private clientItemWishURL = 'http://localhost:8080/client_item_wishes';

  getClientItemWishes(): Observable<ClientItemWish[]> {
    return this.http.get(this.clientItemWishURL).map(data => data.json());
  }

  getClientItemWishByID(id: number): Observable<ClientItemWish> {
    const url = `${this.clientItemWishURL}/${id}`;
    return this.http.get(url).map(data => data.json());
  }

  deleteClientItemWish(id: number): Observable<ClientItemWish> {
    const url = `${this.clientItemWishURL}/deactivate/${id}`;
    return this.http.post(url, {}).map(data => data.json());
  }

  saveClientItemWish(clientItemWish: ClientItemWish): Observable<ClientItemWish> {
    const url = `${this.clientItemWishURL}/save`;
    return this.http.post(url, JSON.stringify(clientItemWish), {headers: this.headers}).map(data => data.json());
  }

  updateClientItemWish(clientItemWish: ClientItemWish): Observable<ClientItemWish> {
    const url = `${this.clientItemWishURL}/update`;
    return this.http.post(url, JSON.stringify(clientItemWish), {headers: this.headers}).map(data => data.json());
  }

}