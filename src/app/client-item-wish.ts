import { Client } from './client';
import { FoundItem } from './found-item'

export class ClientItemWish {
  constructor(
    public id: number,
    public version: number,
    public creationTime: Date,
    public modificationTime: Date,
    public foundItem: FoundItem,
    public client: Client,
    public clientRating: number
  ) { }

}