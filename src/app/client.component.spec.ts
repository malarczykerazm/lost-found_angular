import { ComponentFixture } from '@angular/core/testing';
import { TestBed } from '@angular/core/testing';
import { async, fakeAsync, tick } from '@angular/core/testing';
import { HttpModule, ConnectionBackend, RequestOptions, Http, BaseRequestOptions } from '@angular/http'
import { MockBackend } from "@angular/http/testing";

import { FormsModule } from '@angular/forms';
import { RatingModule } from './../../node_modules/ngx-rating/index.js';

import { ClientComponent } from './client.component';
import { ClientModalComponent } from './client.modal.component';
import { ClientItemWishComponent } from './client-item-wish.component';
import { ClientPipe } from './client.pipe';

import { EnumService } from './enum.service';
import { ClientService } from './client.service';
import { FoundItemService } from './found-item.service';
import { AddressService } from './address.service';
import { ContactDataService } from './contact-data.service';
import { ValidationProcessingService } from './app.validation-processing.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { Client } from './client';
import { Address } from './address';
import { ContactData } from './contact-data';

describe('Should', () => {
    //given
    let comp: ClientComponent;
    let fixture: ComponentFixture<ClientComponent>;
    let enumService: EnumService;
    let clientService: ClientService;
    let addressService: AddressService;

    let testCountries: string[] = ['TestCountry1', 'TestCountry2'];
    let testWislist: ClientItemWishComponent[] = [];

    let testID1: number = 1;

    let testAddress1: Address;
    let testAddress2: Address;
    let testAddress3: Address;
    let testAddresses: Address[];

    let testContactData1: ContactData;
    let testContactData2: ContactData;
    let testContactData3: ContactData;
    let testContactData4: ContactData;

    let testClient1: Client;
    let testClient2: Client;
    let testClient3: Client;
    let testClient4: Client;
    let testClients: Client[];


    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RatingModule, FormsModule, HttpModule],
            declarations: [ClientComponent, ClientModalComponent, ClientPipe],
            providers: [ClientService, EnumService, FoundItemService, AddressService, ContactDataService, ValidationProcessingService],
        }).compileComponents();
    }));

    beforeEach(() => {
        //given
        testAddress1 = new Address(1, 0, new Date(), new Date(), "1a", "TestStreet1", "TestPostcode1", "TestCity1", "TestCountry1");
        testAddress2 = new Address(2, 0, new Date(), new Date(), "2b", "TestStreet2", "TestPostcode2", "TestCity2", "TestCountry2");
        testAddress3 = new Address(3, 0, new Date(), new Date(), "3c", "TestStreet3", "TestPostcode3", "TestCity3", "TestCountry3");
        testAddresses = [testAddress1, testAddress2, testAddress3];

        testContactData1 = new ContactData(1, 0, new Date(), new Date(), "testPhone1", "testEmail1");
        testContactData2 = new ContactData(2, 0, new Date(), new Date(), "testPhone2", "testEmail2");
        testContactData3 = new ContactData(3, 0, new Date(), new Date(), "testPhone3", "testEmail3");
        testContactData4 = new ContactData(4, 0, new Date(), new Date(), "testPhone4", "testEmail4");

        testClient1 = new Client(1, 0, new Date(), new Date(), "TestFirstname1", "TestSurname1", "TestSocialSecurityNo1", testAddress1, testContactData1);
        testClient2 = new Client(2, 0, new Date(), new Date(), "TestFirstname2", "TestSurname2", "TestSocialSecurityNo2", testAddress2, testContactData2);
        testClient3 = new Client(3, 0, new Date(), new Date(), "TestFirstname3", "TestSurname3", "TestSocialSecurityNo3", testAddress3, testContactData3);
        testClient4 = new Client(4, 0, new Date(), new Date(), "TestFirstname4", "TestSurname4", "TestSocialSecurityNo4", testAddress1, testContactData4);
        testClients = [testClient1, testClient2, testClient3, testClient4];

        fixture = TestBed.createComponent(ClientComponent);
        comp = fixture.componentInstance;

        enumService = fixture.debugElement.injector.get(EnumService);
        clientService = fixture.debugElement.injector.get(ClientService);
        addressService = fixture.debugElement.injector.get(AddressService);

    });

    it('call enum service to get all countries', () => {
        //given
        spyOn(enumService, 'getAllCountries').and.returnValue(Observable.of(testCountries));

        //when
        comp.getCountries();

        //then
        expect(enumService.getAllCountries).toHaveBeenCalled();
    });

    it('call client service to get one client`s wishlist', () => {
        //given
        spyOn(clientService, 'getClientWishlist').and.returnValue(Observable.of(testWislist));

        //when
        comp.getClientWishlist(testID1);

        //then
        expect(clientService.getClientWishlist).toHaveBeenCalledWith(testID1);
    });

    it('delete one client, but keep the address', () => {
        //given
        comp.clients = testClients;
        comp.addresses = testAddresses;
        spyOn(window, 'confirm').and.returnValue(true);
        spyOn(clientService, 'deleteClient').and.returnValue(Observable.of(testClient1));

        //when
        comp.deleteClientIfUserIsSure(testClient1.id);

        //then
        expect(window.confirm).toHaveBeenCalled();
        expect(clientService.deleteClient).toHaveBeenCalledWith(testClient1.id);
        expect(comp.clients).toEqual(testClients.filter(c => c.id !== testClient1.id));
        expect(comp.addresses).toEqual(testAddresses);
    });

    it('delete one client with the associated address', () => {
        //given
        comp.clients = testClients;
        comp.addresses = testAddresses;
        spyOn(window, 'confirm').and.returnValue(true);
        spyOn(clientService, 'deleteClient').and.returnValue(Observable.of(testClient2));

        //when
        comp.deleteClientIfUserIsSure(testClient2.id);

        //then
        expect(window.confirm).toHaveBeenCalled();
        expect(clientService.deleteClient).toHaveBeenCalledWith(testClient2.id);
        expect(comp.clients).toEqual(testClients.filter(c => c.id !== testClient2.id));
        expect(comp.addresses).toEqual(testAddresses.filter(a => a.id !== testClient2.address.id));
    });

    it('not delete client due to user`s decision', () => {
        //given
        comp.clients = testClients;
        comp.addresses = testAddresses;
        spyOn(window, 'confirm').and.returnValue(false);
        spyOn(clientService, 'deleteClient').and.returnValue(Observable.of(testClient2));

        //when
        comp.deleteClientIfUserIsSure(testClient2.id);

        //then
        expect(window.confirm).toHaveBeenCalled();
        expect(clientService.deleteClient).not.toHaveBeenCalled();
        expect(comp.clients).toEqual(testClients);
        expect(comp.addresses).toEqual(testAddresses);
    });

    it('save new client with a new address', () => {
        //given
        let freshAddress = new Address(null, null, null, null, "4d", "TestStreet4", "TestPostcode4", "TestCity4", "TestCountry4");
        let freshContactData = new ContactData(null, null, null, null, "testPhone5", "testEmail5");
        let freshClient = new Client(null, null, null, null, "TestFirstname5", "TestSurname5", "TestSocialSecurityNo5", freshAddress, freshContactData);
        comp.clients = testClients;
        comp.addresses = testAddresses;
        let savedAddress = new Address(4, 0, new Date(), new Date(), freshAddress.addressNumber, freshAddress.street, freshAddress.postcode, freshAddress.city, freshAddress.country);
        let savedContactData = new ContactData(5, 0, new Date(), new Date(), freshContactData.phone, freshContactData.email);
        let savedClient = new Client(5, 0, new Date(), new Date(), freshClient.firstname, freshClient.surname, freshClient.socialSecurityNumber, savedAddress, savedContactData)
        spyOn(clientService, 'saveClient').and.returnValue(Observable.of(savedClient));
        spyOn(clientService, 'updateClient');
        spyOn(clientService, 'getClientWishlist').and.returnValue(Observable.of(testWislist));
        spyOn(addressService, 'getAddresses').and.returnValue(Observable.of([testAddress1, testAddress2, testAddress3, savedAddress]));

        //when
        comp.saveOrUpdate(freshClient);

        //then
        expect(clientService.saveClient).toHaveBeenCalledWith(freshClient);
        expect(clientService.updateClient).not.toHaveBeenCalled();
        expect(clientService.getClientWishlist).toHaveBeenCalledWith(savedClient.id);
        expect(addressService.getAddresses).toHaveBeenCalled();
        expect(comp.clients).toEqual([testClient1, testClient2, testClient3, testClient4, savedClient]);
        expect(comp.addresses).toEqual([testAddress1, testAddress2, testAddress3, savedAddress]);
    });

    it('save new client with an existing address', () => {
        //given
        let freshContactData = new ContactData(null, null, null, null, "testPhone5", "testEmail5");
        let freshClient = new Client(null, null, null, null, "TestFirstname5", "TestSurname5", "TestSocialSecurityNo5", testAddress1, freshContactData);
        comp.clients = testClients;
        comp.addresses = testAddresses;
        let savedContactData = new ContactData(5, 0, new Date(), new Date(), freshContactData.phone, freshContactData.email);
        let savedClient = new Client(5, 0, new Date(), new Date(), freshClient.firstname, freshClient.surname, freshClient.socialSecurityNumber, testAddress1, savedContactData)
        spyOn(clientService, 'saveClient').and.returnValue(Observable.of(savedClient));
        spyOn(clientService, 'updateClient');
        spyOn(clientService, 'getClientWishlist').and.returnValue(Observable.of(testWislist));
        spyOn(addressService, 'getAddresses');

        //when
        comp.saveOrUpdate(freshClient);

        //then
        expect(clientService.saveClient).toHaveBeenCalledWith(freshClient);
        expect(clientService.updateClient).not.toHaveBeenCalled();
        expect(clientService.getClientWishlist).toHaveBeenCalledWith(savedClient.id);
        expect(addressService.getAddresses).not.toHaveBeenCalled();
        expect(comp.clients).toEqual([testClient1, testClient2, testClient3, testClient4, savedClient]);
        expect(comp.addresses).toEqual(testAddresses);
    });
    
    it('update the client and save a new address', () => {
        //given
        let freshAddress = new Address(null, null, null, null, "4d", "TestStreet4", "TestPostcode4", "TestCity4", "TestCountry4");
        let freshContactData = new ContactData(null, null, null, null, "testPhone5", "testEmail5");
        let freshClient = new Client(4, 0, new Date(), new Date(), "TestFirstname5", "TestSurname5", "TestSocialSecurityNo5", freshAddress, freshContactData);
        comp.clients = testClients;
        comp.addresses = testAddresses;
        let savedAddress = new Address(4, 0, new Date(), new Date(), freshAddress.addressNumber, freshAddress.street, freshAddress.postcode, freshAddress.city, freshAddress.country);
        let savedContactData = new ContactData(4, 0, new Date(), new Date(), freshContactData.phone, freshContactData.email);
        let savedClient = new Client(4, 1, new Date(), new Date(), freshClient.firstname, freshClient.surname, freshClient.socialSecurityNumber, savedAddress, savedContactData);
        spyOn(clientService, 'saveClient');
        spyOn(clientService, 'updateClient').and.returnValue(Observable.of(savedClient));
        spyOn(clientService, 'getClientWishlist').and.returnValue(Observable.of(testWislist));
        spyOn(addressService, 'getAddresses').and.returnValue(Observable.of([testAddress1, testAddress2, testAddress3, savedAddress]));

        //when
        comp.saveOrUpdate(freshClient);

        //then
        expect(clientService.saveClient).not.toHaveBeenCalled();
        expect(clientService.updateClient).toHaveBeenCalledWith(freshClient);
        expect(clientService.getClientWishlist).toHaveBeenCalledWith(savedClient.id);
        expect(addressService.getAddresses).toHaveBeenCalled();
        expect(comp.clients).toEqual([testClient1, testClient2, testClient3, savedClient]);
        expect(comp.addresses).toEqual([testAddress1, testAddress2, testAddress3, savedAddress]);
    });
        
    it('update the client with an existing address', () => {
        //given
        let freshAddress = new Address(1, 0, new Date(), new Date(), "1a", "TestStreet1", "TestPostcode1", "TestCity1", "TestCountry1");
        let freshContactData = new ContactData(null, null, null, null, "testPhone5", "testEmail5");
        let freshClient = new Client(4, 0, new Date(), new Date(), "TestFirstname5", "TestSurname5", "TestSocialSecurityNo5", freshAddress, freshContactData);
        comp.clients = testClients;
        comp.addresses = testAddresses;
        let savedAddress = freshAddress;
        let savedContactData = new ContactData(4, 0, new Date(), new Date(), freshContactData.phone, freshContactData.email);
        let savedClient = new Client(4, 1, new Date(), new Date(), freshClient.firstname, freshClient.surname, freshClient.socialSecurityNumber, savedAddress, savedContactData);
        spyOn(clientService, 'saveClient');
        spyOn(clientService, 'updateClient').and.returnValue(Observable.of(savedClient));
        spyOn(clientService, 'getClientWishlist').and.returnValue(Observable.of(testWislist));
        spyOn(addressService, 'getAddresses');

        //when
        comp.saveOrUpdate(freshClient);

        //then
        expect(clientService.saveClient).not.toHaveBeenCalled();
        expect(clientService.updateClient).toHaveBeenCalledWith(freshClient);
        expect(clientService.getClientWishlist).toHaveBeenCalledWith(savedClient.id);
        expect(addressService.getAddresses).not.toHaveBeenCalled();
        expect(comp.clients).toEqual([testClient1, testClient2, testClient3, savedClient]);
        expect(comp.addresses).toEqual(testAddresses);
    });

});