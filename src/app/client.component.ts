import { Component, OnInit } from '@angular/core';

import { Client } from './client';
import { ClientItemWish } from './client-item-wish';
import { FoundItem } from "./found-item";
import { Address } from "./address";
import { ContactData } from "./contact-data";

import { ClientService } from './client.service';
import { EnumService } from './enum.service';
import { FoundItemService } from './found-item.service'
import { AddressService } from './address.service'
import { ContactDataService } from './contact-data.service'
import { ValidationProcessingService } from './app.validation-processing.service';

import { ClientPipe } from './client.pipe';

@Component({
  moduleId: module.id,
  templateUrl: `./../client.component.html`,
  styleUrls: [`./../client.component.css`, `./../app.common.modal.component.css`],
})

export class ClientComponent implements OnInit {

  subname = 'Clients';
  detailName = 'Details of the chosen client';
  selectedClient: Client;
  selectedItem: FoundItem;
  clients: Client[];
  clientItemWishes: ClientItemWish[] = [];
  countries: string[];
  addresses: Address[];

  idSorting: Boolean = true;
  firstnameSorting: Boolean = true;
  surnameSorting: Boolean = true;
  citySorting: Boolean = true;

  constructor(
    private clientService: ClientService,
    private enumService: EnumService,
    private foundItemService: FoundItemService,
    private addressService: AddressService,
    private contactDataService: ContactDataService,
    private valProcService: ValidationProcessingService) { }

  getCountries(): void {
    this.enumService.getAllCountries().subscribe(countries => this.countries = countries);
  }

  getAddresses(): void {
    this.addressService.getAddresses().subscribe(a => this.addresses = a);
  }

  getClients(): void {
    this.clientService.getClients().subscribe(c => this.clients = c);
  }

  getClientWishlist(id: number): void {
    this.clientService.getClientWishlist(id).subscribe(cIW => {
      this.clientItemWishes = cIW
    });
  }

  private deleteClient(id: number): void {
    this.clientService.deleteClient(id).subscribe(c => {
      if (this.clients.filter(c => c.address.id === this.clients.find(c => c.id === id).address.id).length === 1) {
        this.addresses = this.addresses.filter(a => a.id !== this.clients.find(c => c.id === id).address.id);
      }
      this.clients = this.clients.filter(c => c.id !== id);
      this.selectedClient = null;
    });
  }

  deleteClientIfUserIsSure(id: number): void {
    if (window.confirm("Are you sure you want to remove the client of ID: " + id + "?")) {
      this.deleteClient(id);
    }
  }

  prepareEmtyClientToSave(): void {
    this.selectedClient = new Client(null, null, null, null, null, null, null, null, null);
    this.setDummyAdderssAndContactDataToClient(this.selectedClient);
  }

  onClientSelect(client: Client): void {
    this.selectedClient = client;
    this.getClientWishlist(client.id);
  }

  offClientSelect(): void {
    this.selectedClient = null;
  }

  setClientItemWishesToEmpty() {
    this.clientItemWishes = [];
  }

  private addAndRefreshClient(client: Client): void {
    this.clientService.saveClient(client).subscribe(c => {
      this.clients.push(c);
      this.selectedClient = c;
      this.getClientWishlist(c.id);
      if (client.address.id === null) {
        this.getAddresses();
      }
      this.setDummyAdderssAndContactDataToClient(this.selectedClient);
    });
  }

  private updateAndRefreshClient(client: Client): void {
    this.clientService.updateClient(client).subscribe(c => {
      var arrayIndex: number = this.clients.indexOf(this.clients.find(c => c.id === client.id));
      c.version++;
      this.clients[arrayIndex] = c;
      this.selectedClient = c;
      this.getClientWishlist(c.id);
      if (client.address.id === null) {
        this.getAddresses();
      }
      this.setDummyAdderssAndContactDataToClient(this.selectedClient);
    });
  }

  justRefreshClient(client: Client): void {
    if (client.id !== null) {
      this.clientService.getClientByID(client.id).subscribe(c => {
        var arrayIndex: number = this.clients.indexOf(this.clients.find(c => c.id === client.id));
        this.clients[arrayIndex] = c;
        this.selectedClient = c;
        this.setDummyAdderssAndContactDataToClient(this.selectedClient);
      });
    }
  }

  saveOrUpdate(client: Client): void {
    if (client.id === null) {
      this.addAndRefreshClient(client);
    } else {
      this.updateAndRefreshClient(client);
    }
  }

  private setDummyAdderssAndContactDataToClient(client: Client): void {
    if (client.address === null) {
      client.address = new Address(null, null, null, null, null, null, null, null, null);
    }
    if (client.contactData === null) {
      client.contactData = new ContactData(null, null, null, null, null, null);
    }
  }

  ngOnInit(): void {
    this.getClients();
    this.getCountries();
    this.getAddresses();
  }

  sortByID() {
    if (this.idSorting) {
      this.clients.sort((c1, c2) => {
        return c1.id < c2.id ? -1 : 1;
      });
      this.idSorting = false;
    } else {
      this.clients.sort((c1, c2) => {
        return c1.id > c2.id ? -1 : 1;
      });
      this.idSorting = true;
    }
  }

  sortByFirstname() {
    if (this.firstnameSorting) {
      this.clients.sort((c1, c2) => {
        return c1.firstname.toLowerCase() < c2.firstname.toLowerCase() ? -1 : 1;
      });
      this.firstnameSorting = false;
    } else {
      this.clients.sort((c1, c2) => {
        return c1.firstname.toLowerCase() > c2.firstname.toLowerCase() ? -1 : 1;
      });
      this.firstnameSorting = true;
    }
  }

  sortBySurname() {
    if (this.surnameSorting) {
      this.clients.sort((c1, c2) => {
        return c1.surname.toLowerCase() < c2.surname.toLowerCase() ? -1 : 1;
      });
      this.surnameSorting = false;
    } else {
      this.clients.sort((c1, c2) => {
        return c1.surname.toLowerCase() > c2.surname.toLowerCase() ? -1 : 1;
      });
      this.surnameSorting = true;
    }
  }

  sortByCity() {
    if (this.citySorting) {
      this.clients.sort((c1, c2) => {
        return c1.address.city.toLowerCase() < c2.address.city.toLowerCase() ? -1 : 1;
      });
      this.citySorting = false;
    } else {
      this.clients.sort((c1, c2) => {
        return c1.address.city.toLowerCase() > c2.address.city.toLowerCase() ? -1 : 1;
      });
      this.citySorting = true;
    }
  }

}