import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { Client } from './client';

@Pipe({ name: 'clientFilter' })
@Injectable()
export class ClientPipe implements PipeTransform {

    transform(clients: any[], searchText: any): any {
        if (searchText == null || searchText === undefined || searchText == []) {
            return clients;
        } else {
            return clients.filter(clients =>
                (clients.id.toString().includes(searchText.toLowerCase())) ||
                (clients.firstname.toLowerCase().includes(searchText.toLowerCase())) ||
                (clients.surname.toLowerCase().includes(searchText.toLowerCase())) ||
                (clients.address.city.toLowerCase().includes(searchText.toLowerCase())));
        }
    }
}