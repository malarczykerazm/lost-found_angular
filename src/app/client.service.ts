import { Injectable } from '@angular/core';

import { Client } from './client';
import { ClientItemWish } from './client-item-wish';

import { Headers, Http } from '@angular/http';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class ClientService {
  constructor(private http: Http) { }

  private headers = new Headers({ 'Content-Type': 'application/json' });

  private clientsURL = 'http://localhost:8080/clients';

  getClients(): Observable<Client[]> {
    return this.http.get(this.clientsURL).map(data => data.json());
  }
  
  getClientByID(id: number): Observable<Client> {
    const url = `${this.clientsURL}/${id}`;
    return this.http.get(url).map(data => data.json());
  }

  deleteClient(id: number): Observable<Client> {
    const url = `${this.clientsURL}/deactivate/${id}`;
    return this.http.post(url, {}).map(data => data.json());
  }

  getClientWishlist(id: number): Observable<ClientItemWish[]> {
    const url = `${this.clientsURL}/${id}/wishes`;
    return this.http.get(url).map(data => data.json());
  }

  updateClient(client: Client): Observable<Client> {
    const url = `${this.clientsURL}/update`;
    return this.http.post(url, JSON.stringify(client), {headers: this.headers}).map(data => data.json());
  }

  saveClient(client: Client): Observable<Client> {
    const url = `${this.clientsURL}/save`;
    return this.http.post(url, JSON.stringify(client), {headers: this.headers}).map(data => data.json());
  }

}