import { Address } from './address';
import { ContactData } from './contact-data';

export class Client {
  constructor(
    public id: number,
    public version: number,
    public creationTime: Date,
    public modificationTime: Date,
    public firstname: string,
    public surname: string,
    public socialSecurityNumber: string,
    public address: Address,
    public contactData: ContactData,
  ) {  }
}