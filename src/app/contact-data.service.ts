import { Injectable } from '@angular/core';

import { ContactData } from './contact-data';

import { Headers, Http } from '@angular/http';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class ContactDataService {
  constructor(private http: Http) { }

  private contactDataURL = 'http://localhost:8080/contact_data';

  getContactData(): Observable<ContactData[]> {
    return this.http.get(this.contactDataURL).map(data => data.json());
  }
}