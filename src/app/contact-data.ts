export class ContactData {
    constructor(
      public id: number,
      public version: number,
      public creationTime: Date,
      public modificationTime: Date,
      public phone: string,
      public email: string
    ) {  }
  }
