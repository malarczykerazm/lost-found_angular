import { Injectable } from '@angular/core';

import { Headers, Http, Response } from '@angular/http';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class EnumService {
  constructor(private http: Http) { }

  getCategories() {
    return ['Horror', 'Comedy', 'Science-Fiction', 'Drama'];
  }

  private enumsUrl = 'http://localhost:8080/enums';

  getAllCountries(): Observable<string[]> {
    const url = `${this.enumsUrl}/countries`;
    return this.http.get(url).map(data => data.json());
  }

  getAllCategories(): Observable<string[]> {
    const url = `${this.enumsUrl}/categories`;
    return this.http.get(url).map(data => data.json());
  }
  
  getAllColours(): Observable<string[]> {
    const url = `${this.enumsUrl}/colours`;
    return this.http.get(url).map(data => data.json());
  }
  
  getAllStatuses(): Observable<string[]> {
    const url = `${this.enumsUrl}/statuses`;
    return this.http.get(url).map(data => data.json());
  }


}