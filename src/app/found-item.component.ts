import { Component, OnInit } from '@angular/core';

import { Client } from './client';
import { ClientItemWish } from './client-item-wish';
import { FoundItem } from "./found-item";
import { Address } from "./address";

import { ClientService } from './client.service';
import { EnumService } from './enum.service';
import { FoundItemService } from './found-item.service';
import { ValidationProcessingService } from './app.validation-processing.service';

import { FoundItemPipe } from './found-item.pipe';

@Component({
  moduleId: module.id,
  templateUrl: `./../found-item.component.html`,
  styleUrls: [`./../found-item.component.css`, `./../app.common.modal.component.css`],
})

export class FoundItemComponent implements OnInit {

  subname = 'Found items';
  detailName = 'Details of the chosen item';
  selectedFoundItem: FoundItem;
  foundItems: FoundItem[];
  clients: Client[];
  foundItemClientWishes: ClientItemWish[] = [];
  categories: string[];
  colours: string[];
  statuses: string[];
  previousItemStatus: string;
  isAwaitingTimeLongEnough: Boolean = true;
  isThisTheFirstClientToCollect: Boolean;

  idSorting: Boolean = true;
  itemNameSorting: Boolean = true;
  categorySorting: Boolean = true;
  statusSorting: Boolean = true;

  constructor(
    private clientService: ClientService,
    private enumService: EnumService,
    private foundItemService: FoundItemService,
    private valProcService: ValidationProcessingService) { }

  getItemCategories(): void {
    this.enumService.getAllCategories().subscribe(cat => this.categories = cat);
  }

  getItemColours(): void {
    this.enumService.getAllColours().subscribe(col => this.colours = col);
  }

  getItemStatuses(): void {
    this.enumService.getAllStatuses().subscribe(s => this.statuses = s);
  }

  getClients(): void {
    this.clientService.getClients().subscribe(c => this.clients = c);
  }

  getFoundItems(): void {
    this.foundItemService.getFoundItems().subscribe(fi => {
      this.foundItems = fi;
    });
  }

  getFoundItemWishlist(id: number): void {
    this.foundItemService.getFoundItemWishlist(id).subscribe(fICW => {
      this.foundItemClientWishes = fICW.sort((fICW1, fICW2) => {
        return fICW1.id < fICW2.id ? -1 : 1;
      });
    });
  }

  private deleteFoundItem(id: number): void {
    this.foundItemService.deleteFoundItem(id).subscribe(fi => {
      this.foundItems = this.foundItems.filter(fi => fi.id !== id);
      this.selectedFoundItem = null;
    });
  }

  deleteFoundItemIfUserIsSure(id: number): void {
    if (window.confirm("Are you sure you want to remove the found item of ID: " + id + "?")) {
      this.deleteFoundItem(id);
    }
  }

  prepareEmtyFoundItemToSave(): void {
    this.selectedFoundItem = new FoundItem(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    this.setDummyClientsWhereNoClients(this.selectedFoundItem);
  }

  onFoundItemSelect(foundItem: FoundItem): void {
    this.selectedFoundItem = foundItem;
    this.getFoundItemWishlist(foundItem.id);
    this.setDummyClientsWhereNoClients(this.selectedFoundItem);
    this.previousItemStatus = this.selectedFoundItem.status;
    this.getFirstClientToCollect(foundItem);
  }

  offFoundItemSelect(): void {
    this.selectedFoundItem = null;
  }

  setFoundItemClientWishesToEmpty() {
    this.foundItemClientWishes = [];
  }

  addAndRefreshFoundItem(foundItem: FoundItem): void {
    this.foundItemService.saveFoundItem(foundItem).subscribe(fi => {
      this.foundItems.push(fi);
      this.selectedFoundItem = fi;
      this.getFoundItemWishlist(fi.id);
      this.setDummyClientsWhereNoClients(this.selectedFoundItem);
    });
  }

  updateAndRefreshFoundItem(foundItem: FoundItem): void {
    this.foundItemService.updateFoundItem(foundItem).subscribe(fi => {
      var arrayIndex: number = this.foundItems.indexOf(this.foundItems.find(fi => fi.id === foundItem.id));
      fi.version++;
      this.foundItems[arrayIndex] = fi;
      this.selectedFoundItem = fi;
      this.getFoundItemWishlist(fi.id);
      this.setDummyClientsWhereNoClients(this.selectedFoundItem);
    });
  }

  justRefreshFoundItem(foundItem: FoundItem): void {
    if (foundItem.id !== null) {
      this.foundItemService.getFoundItemByID(foundItem.id).subscribe(fi => {
        var arrayIndex: number = this.foundItems.indexOf(this.foundItems.find(fi => fi.id === foundItem.id));
        this.foundItems[arrayIndex] = fi;
        this.selectedFoundItem = fi;
        this.setDummyClientsWhereNoClients(this.selectedFoundItem);
      });
    }
  }

  saveOrUpdate(foundItem: FoundItem): void {
    if (foundItem.id === null) {
      this.addAndRefreshFoundItem(foundItem);
    } else {
      this.updateAndRefreshFoundItem(foundItem);
    }
  }

  setDummyClientsWhereNoClients(foundItem: FoundItem): void {
    if (foundItem.reportedByClient === null) {
      foundItem.reportedByClient = new Client(null, null, null, null, null, null, null, null, null);
    }
    if (foundItem.givenToClient === null) {
      foundItem.givenToClient = new Client(null, null, null, null, null, null, null, null, null);
    }
  }

  checkIfFoundItemAwaitingTimeWasLongEnough(foundItem: FoundItem): void {
    if (!foundItem.id) {
      this.isAwaitingTimeLongEnough = true;
    } else if (this.previousItemStatus === 'IN_DEPOSIT' && foundItem.status === 'GIVEN_AWAY') {
      this.isAwaitingTimeLongEnough = this.valProcService.validateFoundItemAwaitingTime(this.foundItems.find(fi => fi.id.toString() === foundItem.id.toString()));
    } else {
      this.isAwaitingTimeLongEnough = true;
    }
  }

  getFirstClientToCollect(item: FoundItem): void {
    if (!item.id || !item.givenToClient.id) {
      this.isThisTheFirstClientToCollect = true;
    } else if (this.previousItemStatus === 'IN_DEPOSIT' && item.status === 'GIVEN_AWAY') {
      this.isThisTheFirstClientToCollect = (item.givenToClient.id.toString() === this.foundItemClientWishes[0].client.id.toString());
    } else {
      this.isThisTheFirstClientToCollect = true;
    }
  }

  ngOnInit(): void {
    this.getFoundItems();
    this.getItemCategories();
    this.getItemColours();
    this.getItemStatuses();
    this.getClients();
  }

  sortByID() {
    if(this.idSorting) {
      this.foundItems.sort((fi1, fi2) => {
        return fi1.id < fi2.id ? -1 : 1;
      });
      this.idSorting = false;
    } else {
      this.foundItems.sort((fi1, fi2) => {
        return fi1.id > fi2.id ? -1 : 1;
      });
      this.idSorting = true;
    }
  }
  
  sortByItemName() {
    if(this.itemNameSorting) {
      this.foundItems.sort((fi1, fi2) => {
        return fi1.itemName.toLowerCase() < fi2.itemName.toLowerCase() ? -1 : 1;
      });
      this.itemNameSorting = false;
    } else {
      this.foundItems.sort((fi1, fi2) => {
        return fi1.itemName.toLowerCase() > fi2.itemName.toLowerCase() ? -1 : 1;
      });
      this.itemNameSorting = true;
    }
  }
  
  sortByCategory() {
    if(this.categorySorting) {
      this.foundItems.sort((fi1, fi2) => {
        return fi1.category.toLowerCase() < fi2.category.toLowerCase() ? -1 : 1;
      });
      this.categorySorting = false;
    } else {
      this.foundItems.sort((fi1, fi2) => {
        return fi1.category.toLowerCase() > fi2.category.toLowerCase() ? -1 : 1;
      });
      this.categorySorting = true;
    }
  }
    
  sortByStatus() {
    if(this.statusSorting) {
      this.foundItems.sort((fi1, fi2) => {
        return fi1.status.toLowerCase() < fi2.status.toLowerCase() ? -1 : 1;
      });
      this.statusSorting = false;
    } else {
      this.foundItems.sort((fi1, fi2) => {
        return fi1.status.toLowerCase() > fi2.status.toLowerCase() ? -1 : 1;
      });
      this.statusSorting = true;
    }
  }

}
