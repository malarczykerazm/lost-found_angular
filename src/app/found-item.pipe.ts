import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { FoundItem } from './found-item';

import { ValidationProcessingService } from './app.validation-processing.service';

@Pipe({ name: 'foundItemFilter' })
@Injectable()
export class FoundItemPipe implements PipeTransform {

    constructor(private vps: ValidationProcessingService) { }
    
    transform(foundItems: any[], searchText: any): any {
        if (searchText == null || searchText === undefined || searchText == []) {
            return foundItems;
        } else {
            return foundItems.filter(foundItems =>
                (foundItems.id.toString().includes(searchText.toLowerCase())) ||
                (foundItems.itemName.toLowerCase().includes(searchText.toLowerCase())) ||
                (this.vps.replaceUnderScores(foundItems.category).toLowerCase().includes(searchText.toLowerCase())) ||
                (this.vps.replaceUnderScores(foundItems.status).toLowerCase().includes(searchText.toLowerCase())));
        }
    }
}