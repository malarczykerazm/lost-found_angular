import { Injectable } from '@angular/core';


import { FoundItem } from './found-item';
import { ClientItemWish } from './client-item-wish';

import { Headers, Http } from '@angular/http';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class FoundItemService {
  constructor(private http: Http) { }

  private headers = new Headers({ 'Content-Type': 'application/json' });

  private foundItemsURL = 'http://localhost:8080/found_items';

  getFoundItems(): Observable<FoundItem[]> {
    return this.http.get(this.foundItemsURL).map(data => data.json());
  }

  getFoundItemByID(id: number): Observable<FoundItem> {
    const url = `${this.foundItemsURL}/${id}`;
    return this.http.get(url).map(data => data.json());
  }

  deleteFoundItem(id: number): Observable<FoundItem> {
    const url = `${this.foundItemsURL}/deactivate/${id}`;
    return this.http.post(url, {}).map(data => data.json());
  }

  getFoundItemWishlist(id: number): Observable<ClientItemWish[]> {
    const url = `${this.foundItemsURL}/${id}/wishes`;
    return this.http.get(url).map(data => data.json());
  }
  
  saveFoundItem(foundItem: FoundItem): Observable<FoundItem> {
    const url = `${this.foundItemsURL}/save`;
    return this.http.post(url, JSON.stringify(foundItem), {headers: this.headers}).map(data => data.json());
  }

  updateFoundItem(foundItem: FoundItem): Observable<FoundItem> {
    const url = `${this.foundItemsURL}/update`;
    return this.http.post(url, JSON.stringify(foundItem), {headers: this.headers}).map(data => data.json());
  }

}