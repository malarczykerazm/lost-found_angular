import { Client } from './client';

export class FoundItem {
  constructor(
    public id: number,
    public version: number,
    public creationTime: Date,
    public modificationTime: Date,
    public itemName: string,
    public category: string,
    public foundWhen: Date,
    public foundWhere: string,
    public colour: string,
    public weight: number,
    public status: string,
    public addDesc?: string,
    public reportedByClient?: Client,
    public outOfDepositSince?: Date,
    public givenToClient?: Client,
  ) { }

}