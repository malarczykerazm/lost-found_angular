import { Component, OnInit } from '@angular/core';

import { Client } from './client';
import { ClientItemWish } from './client-item-wish';
import { LostItem } from "./lost-item";
import { FoundItem } from "./found-item";
import { Address } from "./address";

import { ClientService } from './client.service';
import { EnumService } from './enum.service';
import { LostItemService } from './lost-item.service';
import { FoundItemService } from './found-item.service';
import { ValidationProcessingService } from './app.validation-processing.service';

@Component({
  moduleId: module.id,
  templateUrl: `./../lost-item.component.html`,
  styleUrls: [`./../lost-item.component.css`, `./../app.common.modal.component.css`],
})

export class LostItemComponent implements OnInit {

  subname = 'Lost items';
  detailName = 'Details of the chosen item';
  selectedLostItem: LostItem;
  lostItems: LostItem[];
  clients: Client[];
  foundItems: FoundItem[];
  categories: string[];
  colours: string[];

  idSorting: Boolean = true;
  itemNameSorting: Boolean = true;
  categorySorting: Boolean = true;
  statusSorting: Boolean = true;

  constructor(
    private clientService: ClientService,
    private enumService: EnumService,
    private lostItemService: LostItemService,
    private foundItemService: FoundItemService,
    private valProcService: ValidationProcessingService,
  ) { }

  getItemCategories(): void {
    this.enumService.getAllCategories().subscribe(cat => this.categories = cat);
  }

  getItemColours(): void {
    this.enumService.getAllColours().subscribe(col => this.colours = col);
  }

  getClients(): void {
    this.clientService.getClients().subscribe(c => {
      this.clients = c;
    });
  }

  getFoundItems(): void {
    this.foundItemService.getFoundItems().subscribe(fi => this.foundItems = fi.sort((fi1, fi2) => {
      return fi1.id < fi2.id ? -1 : 1;
    }));
  }

  getLostItems(): void {
    this.lostItemService.getLostItems().subscribe(li => this.lostItems = li);
  }

  private deleteLostItem(id: number): void {
    this.lostItemService.deleteLostItem(id).subscribe(li => {
      this.lostItems = this.lostItems.filter(li => li.id !== id);
      this.selectedLostItem = null;
    });
  }

  deleteLostItemIfUserIsSure(id: number): void {
    if (window.confirm("Are you sure you want to remove the lost item of ID: " + id + "?")) {
      this.deleteLostItem(id);
    }
  }

  prepareEmtyLostItemToSave(): void {
    this.selectedLostItem = new LostItem(null, null, null, null, null, null, null, null, null, null, null, null, null, false);
    this.setDummiesWhereNoClientOrNoFoundItem(this.selectedLostItem);
  }

  onLostItemSelect(lostItem: LostItem): void {
    this.selectedLostItem = lostItem;
    this.setDummiesWhereNoClientOrNoFoundItem(this.selectedLostItem);
  }

  offLostItemSelect(): void {
    this.selectedLostItem = null;
  }

  addAndRefreshLostItem(lostItem: LostItem): void {
    this.lostItemService.saveLostItem(lostItem).subscribe(li => {
      this.lostItems.push(li);
      this.selectedLostItem = li;
      this.setDummiesWhereNoClientOrNoFoundItem(this.selectedLostItem);
    });
  }

  updateAndRefreshLostItem(lostItem: LostItem): void {
    this.lostItemService.updateLostItem(lostItem).subscribe(li => {
      var arrayIndex: number = this.lostItems.indexOf(this.lostItems.find(li => li.id === lostItem.id));
      li.version++;
      this.lostItems[arrayIndex] = li;
      this.selectedLostItem = li;
      this.setDummiesWhereNoClientOrNoFoundItem(this.selectedLostItem);
    });
  }

  justRefreshLostItem(lostItem: LostItem): void {
    if (lostItem.id !== null) {
      this.lostItemService.getLostItemByID(lostItem.id).subscribe(li => {
        var arrayIndex: number = this.lostItems.indexOf(this.lostItems.find(li => li.id === lostItem.id));
        this.lostItems[arrayIndex] = li;
        this.selectedLostItem = li;
        this.setDummiesWhereNoClientOrNoFoundItem(this.selectedLostItem);
      });
    }
  }

  saveOrUpdate(lostItem: LostItem): void {
    if (lostItem.id === null) {
      this.addAndRefreshLostItem(lostItem);
    } else {
      this.updateAndRefreshLostItem(lostItem);
    }
  }

  setDummiesWhereNoClientOrNoFoundItem(lostItem: LostItem): void {
    if (lostItem.reportedByClient === null) {
      lostItem.reportedByClient = new Client(null, null, null, null, null, null, null, null, null);
    }
    if (lostItem.recognizedAsFoundItem === null) {
      lostItem.recognizedAsFoundItem = new FoundItem(null, null, null, null, null, null, null, null, null, null, null);
    }
  }

  ngOnInit(): void {
    this.getLostItems();
    this.getItemCategories();
    this.getItemColours();
    this.getFoundItems();
    this.getClients();
  }

  
  sortByID() {
    if(this.idSorting) {
      this.lostItems.sort((li1, li2) => {
        return li1.id < li2.id ? -1 : 1;
      });
      this.idSorting = false;
    } else {
      this.lostItems.sort((li1, li2) => {
        return li1.id > li2.id ? -1 : 1;
      });
      this.idSorting = true;
    }
  }
  
  sortByItemName() {
    if(this.itemNameSorting) {
      this.lostItems.sort((li1, li2) => {
        return li1.itemName.toLowerCase() < li2.itemName.toLowerCase() ? -1 : 1;
      });
      this.itemNameSorting = false;
    } else {
      this.lostItems.sort((li1, li2) => {
        return li1.itemName.toLowerCase() > li2.itemName.toLowerCase() ? -1 : 1;
      });
      this.itemNameSorting = true;
    }
  }
  
  sortByCategory() {
    if(this.categorySorting) {
      this.lostItems.sort((li1, li2) => {
        return li1.category.toLowerCase() < li2.category.toLowerCase() ? -1 : 1;
      });
      this.categorySorting = false;
    } else {
      this.lostItems.sort((li1, li2) => {
        return li1.category.toLowerCase() > li2.category.toLowerCase() ? -1 : 1;
      });
      this.categorySorting = true;
    }
  }
    
  sortByColour() {
    if(this.statusSorting) {
      this.lostItems.sort((li1, li2) => {
        return li1.colour.toLowerCase() < li2.colour.toLowerCase() ? -1 : 1;
      });
      this.statusSorting = false;
    } else {
      this.lostItems.sort((li1, li2) => {
        return li1.colour.toLowerCase() > li2.colour.toLowerCase() ? -1 : 1;
      });
      this.statusSorting = true;
    }
  }

}
