import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'lost-item-modal',
    templateUrl: `./../app.common.modal.component.html`,
    styleUrls: [`./../app.common.modal.component.css`]
})
export class LostItemModalComponent {

    public visible = false;
    public visibleAnimate = false;

    public show(): void {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }
}