import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { LostItem } from './lost-item';

import { ValidationProcessingService } from './app.validation-processing.service';

@Pipe({ name: 'lostItemFilter' })
@Injectable()
export class LostItemPipe implements PipeTransform {

    constructor(private vps: ValidationProcessingService) { }

    transform(lostItems: any[], searchText: any): any {
        if (searchText == null || searchText === undefined || searchText == []) {
            return lostItems;
        } else {
            return lostItems.filter(lostItems =>
                (lostItems.id.toString().includes(searchText.toLowerCase())) ||
                (lostItems.itemName.toLowerCase().includes(searchText.toLowerCase())) ||
                (this.vps.replaceUnderScores(lostItems.category).toLowerCase().includes(searchText.toLowerCase())) ||
                (this.vps.replaceUnderScores(lostItems.colour).toLowerCase().includes(searchText.toLowerCase())));
        }
    }
}