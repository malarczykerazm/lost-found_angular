import { Injectable } from '@angular/core';


import { LostItem } from './lost-item';
import { ClientItemWish } from './client-item-wish';

import { Headers, Http } from '@angular/http';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class LostItemService {
  constructor(private http: Http) { }

  private headers = new Headers({ 'Content-Type': 'application/json' });

  private lostItemsURL = 'http://localhost:8080/lost_items';

  getLostItems(): Observable<LostItem[]> {
    return this.http.get(this.lostItemsURL).map(data => data.json());
  }

  getLostItemByID(id: number): Observable<LostItem> {
    const url = `${this.lostItemsURL}/${id}`;
    return this.http.get(url).map(data => data.json());
  }

  deleteLostItem(id: number): Observable<LostItem> {
    const url = `${this.lostItemsURL}/deactivate/${id}`;
    return this.http.post(url, {}).map(data => data.json());
  }

  saveLostItem(lostItem: LostItem): Observable<LostItem> {
    const url = `${this.lostItemsURL}/save`;
    return this.http.post(url, JSON.stringify(lostItem), {headers: this.headers}).map(data => data.json());
  }

  updateLostItem(lostItem: LostItem): Observable<LostItem> {
    const url = `${this.lostItemsURL}/update`;
    return this.http.post(url, JSON.stringify(lostItem), {headers: this.headers}).map(data => data.json());
  }

}