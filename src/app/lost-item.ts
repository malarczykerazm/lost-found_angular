import { Client } from './client';
import { FoundItem } from './found-item'

export class LostItem {
  constructor(
    public id: number,
    public version: number,
    public creationTime: Date,
    public modificationTime: Date,
    public itemName: string,
    public category: string,
    public lostWhen: Date,
    public lostWhere: string,
    public colour: string,
    public weight: number,
    public addDesc: string,
    public reportedByClient: Client,
    public recognizedAsFoundItem: FoundItem,
    public isDealCompleted: Boolean,
  ) { }

}